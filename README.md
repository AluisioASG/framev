# framev

_An over-engineered slideshow creator._

I think the above is the best description possible for **framev**.  Images go in, a video goes out.  But what makes it different from simply calling `ffmpeg` yourself?
- Watch: **framev** listen to any changes to files named after the pattern you give.  If any are added, removed or modified, the video is rebuilt after a short while.
- Per-frame framerate: let's say you have specific images you want to keep on the screen for longer (because they have text, for example).  How do you do that with `ffmpeg` alone?

Because of these shortcomings (especially the last one), **framev** was born.
