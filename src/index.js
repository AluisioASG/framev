import fs from 'fs'

import toml from 'toml'
import yargs from 'yargs'

import {Cancellable} from './cancel'
import watch from './watcher'
import buildInputs from './inputs'
import encode from './encoder'


async function doEncode(taskName, cfg, noProgress, cancellationToken) {
  const inputs = await buildInputs(cfg.files.input_pattern, cfg.framelength, cancellationToken)
  if (!inputs.length) {
    // Nothing to do.
    return
  }
  cancellationToken.throwIfCancelled()
  return encode(taskName, inputs, cfg.files.output, cfg.video, noProgress, cancellationToken)
}

const {_: [configFile, ...tasks], ...args} = yargs
  .usage('Usage: $0 [--no-progress] [--encode-only] CONFIG_FILE [TASKS...]')
  .demand(1)
  .boolean('encode-only')
  .boolean('progress').default('progress', true)
  .argv

const config = toml.parse(fs.readFileSync(configFile))
let tasksEntries = Object.entries(config)
if (tasks.length) {
  tasksEntries = tasksEntries.filter(([name]) => Array.includes(tasks, name))
}

if (args.encodeOnly) {
  tasksEntries.reduce(async(promise, [name, cfg]) => {
    await promise
    console.log('Running encoding task %s', name)
    return doEncode(name, cfg, !args.progress, Cancellable.empty)
  }, Promise.resolve()).catch(err => process.nextTick(() => { throw err }))
} else {
  for (let [name, c] of tasksEntries) {
    console.log('Setting up task %s', name)
    watch(c.files.input_pattern, async(cancellationToken) => {
      console.log('Encoding task "%s" triggered', name)
      try {
        await doEncode(name, c, !args.progress, cancellationToken)
        console.log('Encoding task "%s" completed', name)
      } catch (err) {
        console.log('Error running encoding task "%s": %s', name, err.stack || err)
      }
    }, 2000)
  }
}
