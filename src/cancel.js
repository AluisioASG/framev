export Cancellable from 'cancellation'


export class Canceled extends Error {
  toString() {
    return 'Canceled'
  }
}
