import path from 'path'

import braces from 'braces'
import glob from 'glob'

import {Canceled} from './cancel'


const frameLengthCache = new WeakMap()

function perFrameLength(rangedframeLengths) {
  const frameLengths = new Map()
  for (let [range, length] of Object.entries(rangedframeLengths)) {
    if (range === '*') continue
    for (let match of braces(range)) {
      frameLengths.set(match, length)
    }
  }
  return frameLengths
}

function getCachedFrameLengths(flParam) {
  if (frameLengthCache.has(flParam)) {
    return frameLengthCache.get(flParam)
  } else {
    const frameLengths = perFrameLength(flParam)
    frameLengthCache.set(flParam, frameLengths)
    return frameLengths
  }
}


const filenameStemCache = new Map()

function getCachedFilenameStem(filename) {
  if (filenameStemCache.has(filename)) {
    return filenameStemCache.get(filename)
  } else {
    const stem = path.basename(filename, path.extname(filename))
    filenameStemCache.set(filename, stem)
    return stem
  }
}


function globTask(pattern, options, cancellationToken) {
  return new Promise((resolve, reject) => {
      const globber = glob(pattern, options)
        .on('abort', () => reject(new Canceled()))
        .on('error', reject)
        .on('end', resolve)
      cancellationToken.onCancelled(::globber.abort)
  })
}


export default async function buildInputFilesList(pattern, frameLengthsConfig, cancellationToken) {
  const frameLengths = getCachedFrameLengths(frameLengthsConfig)
  const matches = await globTask(pattern, {matchBase: true, nodir: true}, cancellationToken)
  cancellationToken.throwIfCancelled()

  const defaultFrameLength = frameLengthsConfig['*']
  return matches.reduce((out, filename) => {
    const stem = getCachedFilenameStem(filename)
    const framelength = frameLengths.get(stem) || defaultFrameLength
    return out.concat(new Array(framelength).fill(filename))
  }, [])
}
