import fs from 'fs'
import path from 'path'

import ffmpeg from 'fluent-ffmpeg'
import merge2 from 'merge2'
import ProgressBar from 'progress'


export default function encode(taskName, inputFilenames, outputFilename, videoOptions={}, noProgress, cancellationToken) {
  const inputs = inputFilenames.filter(f => f).map(f => fs.createReadStream(f))

  const progress = new ProgressBar(`${taskName} [:bar] :percent :elapsed`, {
    width: process.stdout.columns - taskName.length - 16,
    total: inputs.length
  })

  const inputStream = merge2(...inputs, {objectMode: false})
  const tmpOut = path.join(path.dirname(outputFilename),
                           `.${path.basename(outputFilename)}_${process.pid}.tmp`)
  const encoder = ffmpeg(inputStream)
    .fromFormat('image2pipe')
    .toFormat(videoOptions.format || path.extname(outputFilename).replace(/^\./, ''))
    .videoCodec(videoOptions.codec)
    .outputOptions(videoOptions.params || [])
    .output(tmpOut)

  if (!noProgress) {
    encoder
      .on('progress', ({frames}) => progress.tick(frames - progress.curr))
      .on('error', ::progress.terminate)
      .on('end', ::progress.terminate)
  }

  if (videoOptions.framerate) {
    encoder.inputOptions(`-framerate ${videoOptions.framerate}`)
  }

  cancellationToken.throwIfCancelled()
  return new Promise((resolve, reject) => {
    encoder
      .on('start', cmdline => console.log('ffmpeg command line:', cmdline))
      .on('error', reject)
      .on('end', resolve)
      .run()
    cancellationToken.onCancelled(() => encoder.kill())
  }).then(() => {
    fs.renameSync(tmpOut, outputFilename)
  }).catch((err) => {
    fs.unlinkSync(tmpOut)
    throw err
  })
}
