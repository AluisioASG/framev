import chokidar from 'chokidar'
import debounce from 'debounce'

import {Cancellable} from './cancel'


const promises = new WeakMap()

const emptyPromiseCancel = {
  promise: Promise.resolve(),
  cancel: () => {},
}


export default function watchFiles(pattern, callback, delay=67) {
  function wrapper() {
    const {promise, cancel} = promises.get(callback) || emptyPromiseCancel
    cancel()

    const c = new Cancellable()
    function runCallback() {
      return callback(c.token)
    }
    promises.set(callback, {
      promise: promise.then(runCallback).catch(err => console.error(err.stack || err)),
      cancel: ::c.cancel,
    })
  }

  const wrappedCallback = debounce(wrapper, delay)
  chokidar.watch(pattern)
    .on('add', wrappedCallback)
    .on('unlink', wrappedCallback)
    .on('change', wrappedCallback)
}
